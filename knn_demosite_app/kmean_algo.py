'''
author : simon.micollier@gmail.com
creation date : 28/10/2015
goal : define some classes and methods to manipulate K-MEAN classifier algorithm concepts
'''
# !/usr/bin/python

import csv
import pprint
import random
from operator import attrgetter

from knn_algo import Feature, Node, Nodelist
#from views import createNodelistFromCSV

class ClusterFeature(Feature):

    def __init__(self, name):
        Feature.__init__(self, name)
        self.delta_range = 0

    def setDataRanges(self, extremes):
        for feature, value in extremes.items():
            delta_range = value['emin'] - value['emax'];
        self.delta_range = delta_range


class ClusterNode(Node):

    def __init__(self, type_arg):
        Node.__init__(self, type_arg)
        self.nearest_centroid = []


class Cluster(Nodelist):

    def __init__(self, nodelist, k):
        Nodelist.__init__(self, nodelist, k)
        self.centroids = {}  # struct {id centroid:[centroid_features]}
        self.extremes = []
        # self.k = k  inheritance
        # self.feature_spec inheritance

    def initCentroid(self, featurelist, k):
        map(attrgetter('name'), featurelist)
        if not k:  # k is parameter for number of centroid
            k = 3
        for id_centroid in range(k):
            centroid = {}
            for efeature, value in self.extremes.items():
                #if feature in [feature.name for feature in featurelist]:
                for feature_obj in featurelist:
                    if (feature.name == efeature):
                        centroid[efeature] = value['emin'] + random.randrange(10) *  feature_obj.delta_range
            pp = pprint.PrettyPrinter(indent=4)
            pp.pprint(centroid)
            self.centroids.update({id_centroid:centroid})

    def setNodeCentroid():
        # foreach node we assign the nearest centroid
        for node in self.nodes:
            node_distance = []  # node distance to each centroid
            for id_centroid in range(centroids):
                delta_feature = 0
                for feature in node:
                    delta = node[feature] - centroid[i][feature]
                    delta_feature += delta**2
                node_distance.update({id_centroid:sqrt(delta_feature)})
            node.nearest_centroid = min(enumerate(len(node_distance)), key=lambda i: node_distance)  # assign nearest centroid
            print(node.nearest_centroid)

    def setDataExtremes(self):
        extremes = {}
        for node in self.nodes:
            pp = pprint.PrettyPrinter(indent=4)
            for feature, value in node.features_spec.items():
                if not(extremes.get(feature)):
                    extremes[feature] = {'emin':1000, 'emax':0}
                if (node.features_spec[feature] < extremes[feature]['emin']):
                    extremes[feature]['emin'] = node.features_spec[feature]
                if (node.features_spec[feature] > extremes[feature]['emax']):
                    extremes[feature]['emax'] = node.features_spec[feature]
        pp.pprint(extremes)
        self.extremes = extremes

    def determine_optimized_groupsize():
        # @todo
        print('')

    def determine_node_groups():
        # @todo
        print('')


# Create NodeList class Reference DATASET from knn_algo from a CSV file
def createNodelistFromCSV(path):
    referenceDSnodes = []
    with open(path) as csvfile:
        reader = csv.DictReader(csvfile, ['nodetype', 'feature1', 'feature2', 'spec1', 'spec2'])
        rownum = 0
        for row in reader:
            if rownum != 0:
                currentnode = ClusterNode(row['nodetype'])
                currentnode.add_feature_spec({row['feature1']: int(row['spec1'])})
                currentnode.add_feature_spec({row['feature2']: int(row['spec2'])})
                referenceDSnodes.append(currentnode)
            rownum += 1
        return referenceDSnodes

datasample = [
[1, 2],[2, 1],[2, 4], [1, 3], [2, 2], [3, 1], [1, 1],
[7, 3], [8, 2], [6, 4], [7, 4], [8, 1], [9, 2],
[10, 8], [9, 10], [7, 8], [7, 9], [8, 11], [9, 9]
]

# create a reference dataset
clusternodes = createNodelistFromCSV('./static/simon_dataset.csv')
# create a cluster
cluster = Cluster(clusternodes, 3)

cluster.setDataExtremes()

feature = ClusterFeature('areas')
feature.setDataRanges(cluster.extremes)
feature2 = ClusterFeature('room')
feature2.setDataRanges(cluster.extremes)

cluster.initCentroid([feature,feature2],3)

