'''
author : simon.micollier@gmail.com
creation date : 19/10/2015
goal : define some classes and methods to manipulate K-NN classifier algorithm concepts
'''
# !/usr/bin/python
import copy
from math import sqrt
from operator import attrgetter


# class defining the knn-algo structure for a user dimension like number of rooms, areas, color, ...
class Feature:

    def __init__(self, name, weight=1, helperDimensionFunct=0, helperWeightFunct=0):
        self.name = name
        self.weight = weight
        self.min = 100000
        self.max = 0
        # optim : helperDimensionFunct, to reduce a many dimension value to a 1 dimension value ( e.g RGB 3-values)
        # optim : helperWeightFunct, to calculate weight value (e.g many dimensions like RGB)

    # calculate a feature weight using the range of values extracted from reference nodes
    def refreshfeatureWeight(self, nodelist):
        for node in nodelist.nodes:
            if (self.name in node.features_spec):  # update min/max values if node feature name is matching
                if (node.features_spec[self.name] < self.min):
                    self.min = node.features_spec[self.name]
                if (node.features_spec[self.name] > self.max):
                    self.max = node.features_spec[self.name]
        self.weight = self.max - self.min
        print('min/max', self.min, self.max)


# class defining a node with a type to define or already defined
# e.g a housing type like type appartment, house, or 'toclassify' if the node has to be classified
class Node:

    def __init__(self, type_arg):
        self.features_spec = {}  # a dictionnary of type {feature, value} e.g helpful to get all features from a ref dataset
        self.neighbors = []  # used to store Node neighbors for this curent node
        self.type = type_arg  # node type e.g appartment, house, undefined
        self.type_guessed = ''
        self.distance = 100000

    def debug_node(self):
        print(self.features_spec)
        print(self.neighbors)
        print(self.type)
        print(self.type_guessed)
        print(self.distance)

    def add_feature_spec(self, feature):
        self.features_spec.update(feature)

    def measureDistance(self, featurelist):
        # for all neighbors, calculate a distance score to store in each neighbor
        for i in range(len(self.neighbors)):
            distance = 0
            for feature in featurelist:
                delta_feature = self.neighbors[i].features_spec[feature.name] - self.features_spec[feature]
                delta_feature = (delta_feature) / feature.weight
                distance = distance + delta_feature**2
            self.neighbors[i].distance = sqrt(distance)  # store distance score in neighbor distance field
            print("distance :", self.neighbors[i].distance)

    def sortByDistance(self):
        # order neighbor by distance values
        sorted_neighbors = sorted(self.neighbors, key=attrgetter('distance'))
        self.neighbors = sorted_neighbors

    def guessType(self, k, typelist):
        types_result = {}
        for type_iterator in typelist:
            types_result.update({type_iterator: 0})

        # slice the sorted list based on distance values based on K-Param of KNN algo
        for neighbor in self.neighbors[0: k]:
            # count type frequency in K-neighbors
            if (neighbor.type == 'toclassify'):
                types_result[neighbor.type] = 0
            types_result[neighbor.type] = types_result[neighbor.type]+1

        type_guessed = {'type': False, 'count': 0}
        # extract most pertinent type from types_result
        for type_it in types_result:
            if types_result[type_it] > type_guessed['count']:
                type_guessed['type'] = type_it
                type_guessed['count'] = types_result[type_it]
        self.type_guessed = type_guessed

        print('Guess type', self.type_guessed)
        return self.type_guessed


# a Nodelist of Node like an housing heritage
class Nodelist:

    def __init__(self, nodelist, k):
        self.k = k  # k value for close neighbors constant value for slicing neighbor
        self.nodes = nodelist

    def debug_nodelist(self):
        print(self.k)
        print(self.nodes)

    # as parameter we have list of features to check, list of types choices to determine
    def determineUnknown(self, featurelist, typelist):
        for node in self.nodes:
            if (node.type == 'toclassify'):
                # check distance with his neighbours and copy all neighbours to his neighbour list
                for node2 in self.nodes:
                    if (node2.type != 'toclassify'):
                        node.neighbors.append(copy.copy(node2))
                # measure distance with param option to restrict the list of feature to use
                node.measureDistance(featurelist)
                # sort by distance
                node.sortByDistance()
                # guess node type if node type was toclassify
                type_guessed = node.guessType(self.k, typelist)
        return type_guessed

    def add_node(self, node):
        self.nodes.append(node)


# architecture optimisation to store all features in an Index
class FeaturesIndex:

    # optim : used to store all feature
    def __init__(self):

        print('')

    # @todo Add feature to DB Reference Feature index
    def addfeatureIndex(self, noderef_id, feature, value):

        # @todo
        print('')

    def rmfeatureIndex(self, noderef_id):

        # @todo
        print('')

    def updatefeatureIndex(self, noderef_id, feature, value):

        # @todo
        print('')
